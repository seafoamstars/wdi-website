## Description of the changes
(describe the MR here)

## Checklist:
* [ ] Ran `python manage.py makemigrations` and added resulting migrations
* [ ] Ran `cd whrc && django-admin makemessages` and added resulting translation files
* [ ] Tested the UI on different screen sizes [feel free to remove this if UI is not affected]