# Women's Declaration Website

> :warning: **Please note:** not a lot of time could be spent on this documentation.
Proceed at your own risk, and be aware that you may have to research stuff just to get the site running.

## Set up your development environment

**Note:** To help protect your privacy when contributing to repos, [GitLab allows you to set up a private commit email in your GitLab settings](https://docs.gitlab.com/ee/user/profile/#private-commit-email). If you decide to use this address, copy it and use it when setting the `user.email` config value when configuring your local `git` installation.

### Install prerequisites

#### Mac/Linux 
1. [Install `git`](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) if you don't already have it
2. Ensure `python3` is installed and you have set it as your system's default Python

#### Windows

##### Using the Windows Subsystem for Linux

This is the simpler of the available options and will give you a development environment that's more likely to be compatible with other projects you work on.

1. [Install WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10) with the distribution of your choice (these instructions were tested using Ubuntu 20.04)
2. [Install Python dev tools](https://www.digitalocean.com/community/tutorials/how-to-install-python-3-and-set-up-a-programming-environment-on-an-ubuntu-20-04-server)
3. **Strongly recommended:** for your own sanity, install an IDE that isn't Notepad; try [Visual Studio Code](https://code.visualstudio.com/docs/remote/wsl)
4. **Nice to have:** customize your WSL dev env with some [suggested tools](https://github.com/sirredbeard/Awesome-WSL)
    * A non-default terminal emulator like [ExtraTerm](https://github.com/sedwards2009/extraterm/) or [ConEmu](https://conemu.github.io/) will support tabs and different themes
    * A shell framework like [Bash-It](https://github.com/Bash-it/bash-it) or [ohmyzsh](https://github.com/ohmyzsh/ohmyzsh) makes shell navigation easier

##### Using Windows-native applications

If you don't have a version of Windows 10 that supports WSL, you can still set up a decent development environment.

1. Install [VS Code](https://code.visualstudio.com/download) (not strictly required, but it's one of the better code editors out there)
    * Ensure you set your end-of-line preferences to `\n` (LF/Unix) instead of the default `\r\n` (CRLF/Windows) style
2. Install [Cmder Full](https://cmder.net/) (including Git Bash for Windows)
    * For better organization, change your default startup directory to your Windows user directory (see the first item [here](http://jonathansoma.com/lede/foundations-2019/terminal/setting-up-cmdr-as-windows-shell/) for how to do it)
3. Install [Python 3](https://www.python.org/downloads/windows/) (this comes with `pip`)
    * When the installer prompts, add Python to your `PATH` and disable the `PATH` character limit

### Configure your local dev environment (all OSes)

1. Configure `git`
    * [Set your e-mail address and name](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup)
    * (Windows only) Configure `git` to avoid auto-converting Unix-style line endings to Windows ones with `git config --global core.autocrlf false`
2. [Clone the repository to your local computer](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository)
3. Go to the newly cloned `whrc` repo and install the project packages:
    * Set up your [Python virtual environment](https://realpython.com/python-virtual-environments-a-primer/): `python3 -m venv my-dev-env`
    * Activate your virtual environment
        * Linux/Mac: `source my-dev-env/bin/activate`
        * Windows: `my-dev-env\Scripts\activate`
    * Install the project requirements: `pip install -r requirements.txt`
4. Copy the mock database data to the database used by the app: `cp db_sharable.sqlite3 db.sqlite3`
5. Apply the database migrations: `python manage.py migrate`
6. Start the webserver: `python manage.py runserver`

To deactivate the virtual environment, run `deactivate`.

## Actively developing the website

If you have never worked with Wagtail or Django before, 
[this official tutorial is a good start](https://docs.wagtail.io/en/v2.9/getting_started/tutorial.html). Also,
[this repo](https://github.com/springload/awesome-wagtail) has a great collection of community resources!

To log into Wagtail locally, browse to `/admin` and log in with `wdi`/`wdi_dev_password`.

## Getting your changes merged

1. Make a feature branch from `master` in format <ticket number>-<description-of-feature>. For instance: `256-update-sharable-db`.
2. Make your changes. 
   - If models are affected: run `python manage.py makemigrations` and commit resulting migrations to your branch.
   - If user-facing text is affected: 
      1. Run `cd whrc && django-admin makemessages`;
      2. Locate `.po` files for several languages in folder `whrc/locale`;
      3. Perform the translation of new or changed strings in `.po` files. You can use [Poedit](https://poedit.net/) tool for editing `.po` files. Automated translation with [DeepL](https://deepl.com) is usually fine.
      4. Commit resulting translation files to your branch.
   - If the UI has changed: make sure that the changes are still looking good on mobile, tablet and other screen dimensions. You can use browser's built-in tools for this, or just resize the browser window.
3. Submit an MR with your changes to this repo. 
