from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock
from django.utils.translation import gettext_lazy as _


class SignDeclarationBlock(blocks.StaticBlock):
    def get_context(self, value, parent_context=None):
        from .forms import SignatureWithOrganizationForm

        context = super().get_context(value, parent_context)

        if "sign_declaration_form_data" in context["request"].session:
            form_data = context["request"].session["sign_declaration_form_data"]
            context["form"] = SignatureWithOrganizationForm(form_data)
        else:
            context["form"] = SignatureWithOrganizationForm()

        return context

    class Meta:
        admin_text = _("Sign the Declaration form: no config")
        template = "blocks/sign_dec_form.html"


class DeclarationStatsBlock(blocks.StaticBlock):
    def get_context(self, value, parent_context=None):
        from whrc.models.models import Signature, Organization

        context = super().get_context(value, parent_context)
        context["signature_count"] = Signature.objects.filter(
            email_confirmed=True
        ).count()
        context["organization_count"] = Organization.objects.filter(
            confirmed=True
        ).count()
        context["country_count"] = (
            Signature.objects.filter(email_confirmed=True)
            .values("country")
            .distinct()
            .count()
        )
        return context

    class Meta:
        admin_text = _("Stats about the Declaration: no config")
        template = "blocks/declaration_stats.html"


class OrganizationsListBlock(blocks.StaticBlock):
    def get_context(self, value, parent_context=None):
        from whrc.models.models import Organization

        context = super().get_context(value, parent_context)
        context["organizations_activist"] = Organization.objects.filter(
            confirmed=True, type=Organization.ACTIVIST
        )
        context["organizations_nonprofit"] = Organization.objects.filter(
            confirmed=True, type=Organization.NONPROFIT
        )
        context["organizations_business"] = Organization.objects.filter(
            confirmed=True, type=Organization.BUSINESS
        )
        context["organizations_web"] = Organization.objects.filter(
            confirmed=True, type=Organization.WEB
        )
        context["organizations_other"] = Organization.objects.filter(
            confirmed=True, type=Organization.OTHER
        )

        return context

    class Meta:
        admin_text = (_("Supporting Orgs List: no config"),)
        template = ("blocks/org_list.html",)
        icon = "group"


class BodyBlock(blocks.StreamBlock):
    page_divider = blocks.StaticBlock(
        admin_text=_("Female symbol page divider: no config"),
        template="blocks/female_divider.html",
        icon="horizontalrule",
    )
    declaration_stats = DeclarationStatsBlock(icon="list-ol")
    sign_the_declaration = SignDeclarationBlock(icon="edit")
    basic_paragraph = blocks.StructBlock(
        [
            ("title", blocks.CharBlock()),
            ("body", blocks.RichTextBlock()),
        ],
        help_text=_(
            'A simple paragraph with a title and rich text. For youtube.com embeds, ending the url with "&rel=0" will ensure only videos from the same channel will be advertised at the end.'
        ),
        template="blocks/basic_paragraph.html",
        icon="pilcrow",
    )
    paragraph_with_image = blocks.StructBlock(
        [
            ("title", blocks.CharBlock(required=False)),
            ("body", blocks.RichTextBlock()),
            ("image", ImageChooserBlock()),
        ],
        help_text=_("A paragraph with a title, rich text, and image"),
        template="blocks/paragraph_with_image.html",
        icon="image",
    )
    cta_block = blocks.StructBlock(
        [("button_text", blocks.CharBlock()), ("button_link", blocks.URLBlock())],
        help_text=_("A cta with external link."),
        template="blocks/cta_std.html",
    )
    country_contact_form = blocks.StaticBlock(
        admin_text=_("Country Contact Form: no config"),
        template="blocks/country_contact_form.html",
        icon="form",
    )
    volunteer_form = blocks.StaticBlock(
        admin_text=_("Volunteer Form: no config"),
        template="blocks/volunteer_form.html",
        icon="form",
    )
    donate_widget = blocks.StaticBlock(
        admin_text=_("Donate Widget: no config"),
        template="blocks/donate_widget.html",
        icon="pick",
    )
    org_list = OrganizationsListBlock(icon="group")
    social_media_block = blocks.StructBlock(
        [
            ("spinster_link", blocks.URLBlock(required=False)),
            ("twitter_link", blocks.URLBlock(required=False)),
            ("instagram_link", blocks.URLBlock(required=False)),
            ("telegram_link", blocks.URLBlock(required=False)),
            ("faceBook_link", blocks.URLBlock(required=False)),
            ("youTube_link", blocks.URLBlock(required=False)),
            ("patreon_link", blocks.URLBlock(required=False)),
            ("goFundMe_link", blocks.URLBlock(required=False)),
            ("crowdJustice_link", blocks.URLBlock(required=False)),
            ("website_link", blocks.URLBlock(required=False)),
        ],
        help_text=_("Social media icons with external links."),
        template="blocks/social_media_block.html",
        icon="site",
    )
    embed_resource_block = blocks.StructBlock(
        [
            ("height", blocks.IntegerBlock(default="520")),
            ("embedded_resource_link", blocks.URLBlock()),
            ("direct_resource_link", blocks.URLBlock()),
            ("button_name", blocks.CharBlock()),
            ("source_of_resource", blocks.CharBlock()),
        ],
        help_text=_(
            'Embed an external resource or website. For an embedded google form link, get the url from the src="" portion of the embed option under "Send" in google forms. The button name (e.g. "XX form" or "Watch Video") and source of resource (e.g. "(via Google)" or "(via Youtube)") are for the no-cookie option.'
        ),
        template="blocks/embed_resource_block.html",
        icon="form",
    )


class HeaderCtaBlock(blocks.StreamBlock):
    cta = blocks.StructBlock(
        [
            ("button_text", blocks.CharBlock()),
            ("button_link", blocks.PageChooserBlock()),
        ],
        icon="redirect",
    )
