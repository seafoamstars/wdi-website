from PIL import Image, ImageOps
from io import BytesIO

from captcha.fields import CaptchaField, CaptchaTextInput
from django import forms
from django.forms import ModelForm
from django.utils.translation import gettext_lazy as _

from whrc.models.models import Signature, Organization


class CustomCaptchaTextInput(CaptchaTextInput):
    template_name = "whrc/partials/captcha.html"


class SignatureForm(ModelForm):
    captcha = CaptchaField(
        widget=CustomCaptchaTextInput,
        help_text=_("Enter the letters from the image. Click the image if unreadable"),
    )

    class Meta:
        model = Signature
        fields = [
            "name",
            "email",
            "subscribe_to_emails",
            "organization",
            "sign_for_organization",
            "country",
            "province",
        ]
        widgets = {
            "name": forms.TextInput(attrs={"placeholder": "Jane Doe"}),
            "email": forms.EmailInput(attrs={"placeholder": "jdoe@example.com"}),
            "organization": forms.TextInput(attrs={"placeholder": "BigOrg inc."}),
        }

    def clean(self):
        cleaned_data = super().clean()
        email = cleaned_data.get("email")
        if Signature.objects.filter(email=email).exists():
            self.add_error("email", "Duplicate email")
        return self.cleaned_data

    def save_with_language(self, language_code):
        self.instance.language = language_code
        return super(SignatureForm, self).save()


class SignatureWithOrganizationForm(SignatureForm):
    """
    This form has all the fields for a Signature, plus additional fields for the Organization.
    """

    org_international = forms.BooleanField(
        required=False,
        label=_("International?"),
        help_text=_("Is this organization international? (will override country)"),
    )
    org_type = forms.ChoiceField(
        required=False,
        label=_("Organization type"),
        help_text=_("What does this organization do?"),
        choices=Organization.choices,
    )
    org_logo = forms.ImageField(
        required=False,
        label=_("Organization logo"),
        help_text=_("Upload the logo for your organization here."),
    )
    org_homepage = forms.URLField(
        required=False,
        label=_("Organization homepage"),
        help_text=_("URL for website or social media"),
    )
    org_permission_support = forms.BooleanField(
        required=False,
        label=_("Can we mention support?"),
        help_text=_(
            "The organization gives permission to WDI to mention to policy makers, journalists and "
            "politicians that this organization has signed the Declaration."
        ),
    )
    org_permission_logo = forms.BooleanField(
        required=False,
        label=_("Can we use the logo?"),
        help_text=_(
            "The organization gives permission for WDI to replicate its logo in publicity about the "
            "Declaration, and materials promoting the Declaration."
        ),
    )

    def clean(self):
        super().clean()
        org_logo_dirty = self.cleaned_data.get("org_logo")
        sign_as_org = self.cleaned_data.get("sign_for_organization")

        if sign_as_org:
            clean_image = self.clean_image(org_logo_dirty)
            if not clean_image:
                return
            org_logo_dirty.file = clean_image

            if not self.cleaned_data.get("org_homepage"):
                self.add_error(
                    "org_homepage",
                    _(
                        "If you are signing for your organization, we need your organization's homepage."
                    ),
                )
                return

            if not self.cleaned_data.get("organization"):
                self.add_error(
                    "organization",
                    _(
                        "If you are signing for your organization, we need your organization's name."
                    ),
                )
                return

            if not self.cleaned_data.get("org_type"):
                self.add_error(
                    "organization",
                    _(
                        "If you are signing for your organization, we need your organization's type."
                    ),
                )
                return

        return self.cleaned_data

    def clean_image(self, dirty_logo) -> BytesIO or None:
        if not dirty_logo:
            self.add_error("org_logo", _("The organization logo is in wrong format."))
            return None

        # Get the Pillow Image from bytes array
        image_file = BytesIO(dirty_logo.read())
        image = Image.open(image_file)
        w, h = image.size
        if w > 4096 or h > 4096:
            self.add_error("org_logo", _("The organization logo is too large."))
            return None

        # Save a (possibly) smaller version of a logo
        max_size = 480, 480
        image.thumbnail(max_size)

        # Pad a logo if needed
        min_aspect_ratio = 0.95
        w, h = image.size
        px = image.load()
        first_pixel_color = px[0, 0]
        if w / h < min_aspect_ratio:
            image = ImageOps.pad(
                image, (h, h), method=3, color=first_pixel_color, centering=(0.5, 0.5)
            )
        elif h / w < min_aspect_ratio:
            image = ImageOps.pad(
                image, (w, w), method=3, color=first_pixel_color, centering=(0.5, 0.5)
            )

        image_file = BytesIO()
        image.save(image_file, "PNG", quality=90)

        return image_file

    def save_with_language(self, language_code):
        new_signature = super().save_with_language(language_code)  # Saves the signature

        if new_signature.sign_for_organization:
            new_org = Organization(
                name=self.cleaned_data.get(
                    "organization"
                ),  # Org name from Signature data
                type=self.cleaned_data.get("org_type"),
                # The org can either have a country or be international
                country=self.cleaned_data.get("country")
                if not self.cleaned_data.get("org_international")
                else "",
                international=self.cleaned_data.get("org_international"),
                logo=self.cleaned_data.get("org_logo"),
                homepage=self.cleaned_data.get("org_homepage"),
                contact_email=self.cleaned_data.get(
                    "email"
                ),  # Contact email of the org is the email of its Signatory
                permission_support=self.cleaned_data.get("org_permission_support"),
                permission_logo=self.cleaned_data.get("org_permission_logo"),
                confirmed=False,
            )
            new_org.save()
        else:
            new_org = None

        return new_signature, new_org
