import json

import requests
from django.core.management import BaseCommand
from django.conf import settings

from whrc.models.models import Signature


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.API_key = settings.MAILER_API_KEY

    def add_arguments(self, parser):
        parser.add_argument("country_ids", nargs="+", type=str)

    def handle(self, *args, **options):
        country_ids_to_process = options["country_ids"]

        if any(x.lower() == "all" for x in country_ids_to_process):
            self.process_all_signatures()

        else:
            self.process_signatures_for_countries(country_ids_to_process)

    def which_lists_to_add_to(self, signature: Signature, list_options: list):
        country_list_id = None
        language_list_id = None
        for m in list_options:
            if m["name"] == "Signatories_" + str(signature.country):
                country_list_id = m["id"]
            if m["name"] == "Signatories_" + str(signature.language):
                language_list_id = m["id"]

        lists_to_add_to = []
        if language_list_id:
            lists_to_add_to.append(language_list_id)
        if country_list_id:
            lists_to_add_to.append(country_list_id)

        return lists_to_add_to

    def process_all_signatures(self):
        """
        Synchronizes all the signatures. If an email in signature is not known to the mailer, adds that email to its
        country's mailing list (if present) and a global list of signatories.
        """

        all_signatures = self.get_existing_signatures()
        all_emails = self.get_existing_mails()
        existing_mailing_lists = self.get_all_mail_lists()

        for s in all_signatures:
            if not any(
                    [c["email"].lower() == s.email.lower().strip() for c in all_emails]
            ):
                lists_to_add_to = self.which_lists_to_add_to(s, existing_mailing_lists)

                self.submit_new_email(s, lists_to_add_to)
        self.stdout.write("Processed all new signatures")

    def process_signatures_for_countries(self, country_ids_to_process):
        """
        Synchronizes signatures for selected countries. If an email in signature is not known to the mailer,
        adds that email to its country's mailing list and a global list of signatories.
        If a country doesn't have a mailing list, it will be skipped and a warning will be displayed.
        :param country_ids_to_process: (List) of strings representing countries in Alpha-2 format
        (https://www.iban.com/country-codes)
        """
        existing_mailing_lists = self.get_all_mail_lists()

        for country_id in country_ids_to_process:
            list_id = None
            for m in existing_mailing_lists:
                if m["name"] == "Signatories_" + country_id:
                    list_id = m["id"]

            if not list_id:
                self.stdout.write(
                    "A mailing list for "
                    + country_id
                    + " is missing. Please create a list called Signatories_"
                    + country_id
                )
                self.stdout.write("Skipping signature list: " + country_id)
                continue

            existing_emails = self.get_existing_mails(list_id)
            signatures_on_the_website = self.get_existing_signatures(country_id)

            for s in signatures_on_the_website:
                if not any(
                        [
                            c["email"].lower() == s.email.lower().strip()
                            for c in existing_emails
                        ]
                ):
                    lists_to_add_to = self.which_lists_to_add_to(s, existing_mailing_lists)

                    self.submit_new_email(s, lists_to_add_to)

            self.stdout.write("Processed signature list: " + country_id)

    def get_existing_mails(self, list_id=None):

        if not list_id:
            url = "https://api.sendinblue.com/v3/contacts"
        else:
            url = str.format(
                "https://api.sendinblue.com/v3/contacts/lists/{}/contacts", list_id
            )

        # max return is 1000
        querystring = {"offset": "0", "limit": 500}

        headers = {
            "accept": "application/json",
            "content-type": "application/json",
            "api-key": self.API_key,
        }

        response = requests.request("GET", url, headers=headers, params=querystring)
        if response.status_code != 200:
            self.stderr.write("Problem with getting emails : " + str(response.text))
            return None
        response_json = json.loads(response.text)

        contact_count = response_json["count"]
        all_contacts = response_json["contacts"]

        # paginate
        offset = 0
        while offset + 500 < contact_count:
            offset = offset + 500
            querystring["offset"] = offset
            response = requests.request("GET", url, headers=headers, params=querystring)
            if response.status_code != 200:
                self.stderr.write("Problem with getting emails : " + str(response.text))
                return None

            all_contacts += json.loads(response.text)["contacts"]

        return all_contacts

    def submit_new_email(self, signature, lists_ids=None):
        url = "https://api.sendinblue.com/v3/contacts"

        headers = {
            "accept": "application/json",
            "content-type": "application/json",
            "api-key": self.API_key,
        }

        payload = {
            "updateEnabled": True,
            "email": signature.email.lower().strip(),
            "attributes": {
                "NAME": signature.name,
                "COUNTRY": str(signature.country),
                "ORGANISATION": signature.organization,
                "SIGN_FOR_ORGANISATION": signature.sign_for_organization,
                "LANGUAGE": signature.language,
                "STATE_OR_PROVINCE": signature.province.name
                if signature.province is not None
                else "",
            },
            "listIds": lists_ids + [3],
        }

        response = requests.request(
            "POST", url, data=json.dumps(payload), headers=headers
        )
        if response.status_code not in [201, 204]:
            self.stderr.write(
                "Problem with email " + signature.email + ": " + str(response.text)
            )

    def get_all_mail_lists(self):
        url = "https://api.sendinblue.com/v3/contacts/lists"

        querystring = {"limit": "50", "offset": "0"}

        headers = {
            "accept": "application/json",
            "content-type": "application/json",
            "api-key": self.API_key,
        }

        response = requests.request("GET", url, headers=headers, params=querystring)
        if response.status_code != 200:
            self.stderr.write("Problem with getting mail lists : " + str(response.text))
            return None

        list_count = json.loads(response.text)["count"]
        all_lists = json.loads(response.text)["lists"]

        # paginate
        offset = 0
        while offset + 50 < list_count:
            offset = offset + 50
            querystring["offset"] = offset
            response = requests.request("GET", url, headers=headers, params=querystring)
            if response.status_code != 200:
                self.stderr.write(
                    "Problem with getting mail lists : " + str(response.text)
                )
                return None

            all_lists += json.loads(response.text)["lists"]

        return all_lists

    def get_existing_signatures(self, country_code=None):
        signatures_query = Signature.objects.filter(subscribe_to_emails=True).filter(
            email_confirmed=True
        )

        if country_code:
            signatures_query = signatures_query.filter(country=country_code)

        return signatures_query.all()
