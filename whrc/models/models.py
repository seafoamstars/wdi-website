from datetime import datetime

from django.conf import settings
from django.db import models
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy
from django_countries.fields import CountryField
from smart_selects.db_fields import ChainedForeignKey
from wagtail.contrib.settings.models import BaseSetting, register_setting


class Province(models.Model):
    name = models.CharField(max_length=255, blank=True)

    country = CountryField()

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["country"]


class Signature(models.Model):
    name = models.CharField(
        max_length=255,
        help_text=_("Your first and last name"),
        verbose_name=pgettext_lazy("Signatory name", "Name"),
    )
    email = models.EmailField(
        max_length=1000, unique=True, help_text=_("Your email"), verbose_name=_("Email")
    )
    subscribe_to_emails = models.BooleanField(
        help_text=_("Get email updates?"),
        blank=True,
        verbose_name=_("Subscribe to emails?"),
    )
    organization = models.CharField(
        max_length=255,
        help_text=_("Your affiliated group or company (optional)"),
        blank=True,
        verbose_name=_("Organization"),
    )
    sign_for_organization = models.BooleanField(
        help_text=_("Sign on behalf of this organization?"),
        blank=True,
        verbose_name=_("Sign for organization?"),
    )
    country = CountryField(
        help_text=_("What is your country?"), verbose_name=_("Country")
    )
    province = ChainedForeignKey(
        Province,
        null=True,
        blank=True,
        chained_field="country",
        chained_model_field="country",
        on_delete=models.CASCADE,
        help_text=_("Your state or province (optional)"),
        verbose_name=_("State/Province"),
    )
    timestamp = models.DateTimeField(auto_now_add=True)
    email_confirmed = models.BooleanField(
        help_text=_("Checked if this signatory confirmed their email."),
        blank=True,
        default=False,
        verbose_name=_("Email confirmed"),
    )

    language = models.CharField(
        max_length=12,
        help_text=_("Which language of the website did this user use to sign?"),
        blank=True,
        choices=settings.LANGUAGES,
        verbose_name=_("Language"),
    )

    def __str__(self):
        return f"{self.name} ({self.email})"

    class Meta:
        permissions = [
            ("can_download_signatures", "Can download signatures"),
        ]


class SignatureConfirmationToken(models.Model):
    token = models.CharField(
        max_length=32,
    )
    timestamp = models.DateTimeField(auto_now_add=True)
    signature = models.OneToOneField(
        Signature,
        on_delete=models.CASCADE,
    )


def upload_logos_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/organisation-logos/<datetime>_<org_name>.png
    return "organisation-logos/{0}_{1}.png".format(
        datetime.now().strftime("%Y%m%d_%H:%M:%S"),
        slugify(instance.name),
    )


class Organization(models.Model):
    ACTIVIST = "ACTIVIST"
    WEB = "WEB"
    NONPROFIT = "NONPROFIT"
    BUSINESS = "BUSINESS"
    OTHER = "OTHER"

    choices = [
        (ACTIVIST, _("Activist group")),
        (NONPROFIT, _("Non-activist nonprofit")),
        (BUSINESS, pgettext_lazy("A noun", "Business")),
        (WEB, _("Website, blog, online resource")),
        (OTHER, pgettext_lazy("As in, other kind of Organization", "Other")),
    ]

    name = models.CharField(
        max_length=255,
        help_text=_("Name of the organization"),
        verbose_name=_("Organization name"),
    )

    type = models.CharField(
        max_length=255,
        help_text=_("Type of the organization: activist group, business, ..."),
        verbose_name=_("Organization type"),
        choices=choices,
        default=OTHER,
    )
    country = CountryField(
        help_text=_("Where is this org based?"), blank=True, verbose_name=_("Country")
    )
    international = models.BooleanField(
        verbose_name=_("International"),
        help_text=_("Is this organization international?"),
        blank=True,
    )
    logo = models.ImageField(
        help_text=_("A square or circle logo"),
        verbose_name=_("Logo"),
        upload_to=upload_logos_path,
    )
    homepage = models.URLField(
        max_length=200,
        help_text=_("Link to the organization's homepage"),
        blank=True,
        verbose_name=_("Homepage"),
    )
    contact_email = models.EmailField(
        max_length=1000,
        blank=True,
        help_text=_("Contact email of this organization"),
        verbose_name=_("Contact Email"),
    )
    permission_support = models.BooleanField(
        help_text=_(
            "If the organization gives permission to WDI to mention to policy makers, journalists and "
            "politicians that this organisation has signed the Declaration."
        ),
        default=False,
        verbose_name=_("Permission to mention Support"),
    )
    permission_logo = models.BooleanField(
        help_text=_(
            "If the organization gives permission for WDI to replicate its logo in publicity about the "
            "Declaration, and materials promoting the Declaration."
        ),
        default=False,
        verbose_name=_("Permission to use the Logo"),
    )
    confirmed = models.BooleanField(
        help_text=_(
            "Checked if this organization's data is checked and confirmed, and the organization should "
            "be displayed in the Organizations block"
        ),
        default=False,
        verbose_name=_("Organization data confirmed"),
    )

    def __str__(self):
        return f"{self.name} ({self.type})"


class NewsItem(models.Model):
    headline = models.CharField(
        max_length=255,
        help_text=_("The headline of the article"),
        verbose_name=_("Headline"),
    )
    preview_text = models.TextField(
        max_length=300,
        help_text=_("A few lines of preview text for the article"),
        verbose_name=_("Preview text"),
    )
    image = models.ImageField(
        help_text=_("A square image to represent the post"), verbose_name=_("Image")
    )
    article_link = models.URLField(
        max_length=200,
        help_text=_("Link to the original source"),
        blank=True,
        verbose_name=_("Article link"),
    )
    cta_link = models.URLField(
        max_length=200,
        help_text=_("Call to action button link"),
        blank=True,
        null=True,
        verbose_name=_("CTA link"),
    )
    cta_title = models.CharField(
        max_length=255,
        help_text=_("Prompt the user to take action"),
        blank=True,
        null=True,
        verbose_name=_("CTA title"),
    )
    cta_subtitle = models.CharField(
        max_length=255,
        help_text=_("Prompt the user to take action"),
        blank=True,
        null=True,
        verbose_name=_("CTA subtitle"),
    )
    cta_image = models.ImageField(
        help_text=_("A square or round logo or image representing the call to action"),
        blank=True,
        null=True,
        verbose_name=_("CTA image"),
    )
    timestamp = models.DateTimeField(auto_now_add=True)
    language = models.CharField(
        max_length=12,
        help_text=_("Which language will the user read news in?"),
        blank=True,
        choices=settings.LANGUAGES,
        verbose_name=_("News Language"),
        default="en",
    )

    def __str__(self):
        return self.headline


class Event(models.Model):
    title = models.CharField(
        max_length=255, help_text=_("The title of the event"), verbose_name=_("Title")
    )
    description = models.TextField(
        help_text=_("The description of the event"), verbose_name=_("Description")
    )
    location = models.TextField(
        help_text=_("The location of the event"), verbose_name=_("Location")
    )
    start_time = models.DateTimeField(
        help_text=_("The start time of the event"), verbose_name=_("Start time")
    )
    end_time = models.DateTimeField(
        help_text=_("The start time of the event"), verbose_name=_("End time")
    )
    image = models.ImageField(
        help_text=_("An image that represents the event"), verbose_name=_("Image")
    )
    event_link = models.URLField(
        max_length=200,
        help_text=_("Link to the event signup page"),
        blank=True,
        verbose_name=_("Event link"),
    )


@register_setting
class SocialMediaSettings(BaseSetting):
    facebook = models.URLField(help_text=_("Your Facebook page URL"))
    twitter = models.CharField(max_length=255, help_text=_("Your Twitter profile URL"))
    youtube = models.URLField(help_text=_("Your YouTube channel or user account URL"))
    repo = models.URLField(help_text="The URL to your source code")
    spinster = models.URLField(help_text="Your Spinster account URL")


@register_setting
class UploadSiteLogo(BaseSetting):
    transparent_site_logo = models.ImageField(
        default="/static/images/logo.png",
        help_text="Site logo displayed in headers and footers (transparent images are fine).",
    )
    opaque_site_logo = models.ImageField(
        default="/static/images/purple_logo.png",
        help_text="Site logo used in image previews by other websites linking to this site (transparent images not "
        "recommended).",
    )
