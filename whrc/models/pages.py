from datetime import datetime

from django.core.paginator import Paginator
from django.db import models
from django.utils.translation import gettext_lazy as _
from django_countries import countries
from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core.fields import StreamField
from wagtail.core.models import Page

from whrc.blocks import BodyBlock, HeaderCtaBlock
from whrc.models.models import Signature, NewsItem, Event, Organization


class StandardPage(Page):
    subtitle = models.CharField(
        max_length=255,
        help_text=_("A subtitle to show on the header of the page"),
        blank=True,
        verbose_name=_("Subtitle"),
    )

    header_ctas = StreamField(
        HeaderCtaBlock(required=False), blank=True, verbose_name=_("Header CTAs")
    )
    body = StreamField(BodyBlock(required=False), blank=True, verbose_name=_("Body"))

    content_panels = Page.content_panels + [
        FieldPanel("subtitle"),
        StreamFieldPanel("header_ctas"),
        StreamFieldPanel("body"),
    ]


class CountryListPage(Page):
    subtitle = models.CharField(
        max_length=255,
        help_text=_("A subtitle to show on the header of the page"),
        blank=True,
        verbose_name=_("Subtitle"),
    )

    header_ctas = StreamField(
        HeaderCtaBlock(required=False), blank=True, verbose_name=_("Header CTAs")
    )

    content_panels = Page.content_panels + [
        FieldPanel("subtitle"),
        StreamFieldPanel("header_ctas"),
    ]

    # TODO: cache
    def get_context(self, request):
        context = super().get_context(request)

        all_country_codes = [code for code, name in list(countries)]
        signatures_per_country = {
            c: 0 for c in all_country_codes
        }  # Initialise dictionary with 0 signatures for every country

        signatures = Signature.objects.filter(email_confirmed=True)
        signature_countries = set()

        for signature in signatures:
            signatures_per_country[signature.country] = (
                signatures_per_country[signature.country] + 1
            )
            signature_countries.add(signature.country)

        def getName(item):
            return item.name

        context["countries"] = sorted(signature_countries, key=getName)
        context["signatures_per_country"] = signatures_per_country
        context["country_count"] = len(signature_countries)

        return context


class SignatureListPage(Page):
    subtitle = models.CharField(
        max_length=255,
        help_text=_("A subtitle to show on the header of the page"),
        blank=True,
        verbose_name=_("Subtitle"),
    )

    header_ctas = StreamField(
        HeaderCtaBlock(required=False), blank=True, verbose_name=_("Header CTAs")
    )

    content_panels = Page.content_panels + [
        FieldPanel("subtitle"),
        StreamFieldPanel("header_ctas"),
    ]

    def get_context(self, request):
        context = super().get_context(request)

        signatures_list = Signature.objects.filter(email_confirmed=True).order_by(
            "-timestamp"
        )
        organizations_list = Organization.objects.all().order_by("name")
        paginator = Paginator(signatures_list, 200)

        page = request.GET.get("page")
        signatures = paginator.get_page(page)

        context["signatures"] = signatures
        context["organizations"] = organizations_list
        return context


class NewsItemListPage(Page):
    subtitle = models.CharField(
        max_length=255,
        help_text=_("A subtitle to show on the header of the page"),
        blank=True,
        verbose_name=_("Subtitles"),
    )

    header_ctas = StreamField(
        HeaderCtaBlock(required=False), blank=True, verbose_name=_("Header CTAs")
    )

    content_panels = Page.content_panels + [
        FieldPanel("subtitle"),
        StreamFieldPanel("header_ctas"),
    ]

    def get_context(self, request):
        context = super().get_context(request)

        new_items_list = NewsItem.objects.all().order_by("-timestamp")
        paginator = Paginator(new_items_list, 9)

        page = request.GET.get("page")
        news_items = paginator.get_page(page)

        context["news_items"] = news_items
        return context


class EventListPage(Page):
    subtitle = models.CharField(
        max_length=255,
        help_text=_("A subtitle to show on the header of the page"),
        blank=True,
        verbose_name=_("Subtitle"),
    )

    header_ctas = StreamField(
        HeaderCtaBlock(required=False), blank=True, verbose_name=_("Header CTAs")
    )

    content_panels = Page.content_panels + [
        FieldPanel("subtitle"),
        StreamFieldPanel("header_ctas"),
    ]

    def get_context(self, request):
        context = super().get_context(request)

        events_list = Event.objects.all().order_by("-timestamp")
        now = datetime.now()

        upcoming_events = set()
        past_events = set()

        for e in Event.objects.filter(end_time__gt=now):
            # Without select_related(), this would make a database query for each
            # loop iteration in order to fetch the related blog for each entry.
            # https://docs.djangoproject.com/en/2.2/ref/models/querysets/#select-related
            upcoming_events.add(e)

        for e in Event.objects.filter(end_time__lt=now):
            past_events.add(e)

        context["upcoming_events"] = upcoming_events
        context["past_events"] = past_events
        return context
