from .base import *

DEBUG = False

env = os.environ.copy()

if 'SECRET_KEY' in env:
    SECRET_KEY = env['SECRET_KEY']

if 'ALLOWED_HOSTS' in env:
    ALLOWED_HOSTS = env['ALLOWED_HOSTS'].split(',')

if 'ADMIN_NAME' in env:
    ADMIN_NAME = env['ADMIN_NAME']
else:
    ADMIN_NAME = 'admin'

BASE_URL = 'https://womensdeclaration.com'

# Configure email
# https://docs.djangoproject.com/en/2.0/topics/email/
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS = True
# EMAIL_USE_SSL = True
EMAIL_HOST = env['EMAIL_HOST']
EMAIL_PORT = int(env.get('EMAIL_PORT', 587))
EMAIL_HOST_USER = env['EMAIL_USER']
EMAIL_HOST_PASSWORD = env['EMAIL_PASSWORD']
EMAIL_TIMEOUT = 5

TO_ADDRESS_ORG_REGISTERED = env.get('TO_ADDRESS_ORG_REGISTERED', 'info@womensdeclaration.com')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'ERROR'),
        },
    },
}

# Makes WhiteNoise work properly.
# http://whitenoise.evans.io/en/stable/django.html#django-compressor
COMPRESS_OFFLINE = True
MAILER_API_KEY = env["SENDINBLUE_API_KEY"]

WAGTAIL_SITE_NAME = "Women's Declaration International"
WAGTAIL_2FA_REQUIRED = True

if 'CSP_ALLOWED_CONTENT_SOURCES' in env:
    csp_allowed_sources = env['CSP_ALLOWED_CONTENT_SOURCES'].split(',')
else:
    csp_allowed_sources = []

if 'CSP_SITES_ALLOWED_IFRAME' in env:
    csp_allowed_iframe_sites = env['CSP_SITES_ALLOWED_IFRAME'].split(',')
else:
    csp_allowed_iframe_sites = []

CSP_STYLE_SRC = ["'self'", "'unsafe-inline'"] + csp_allowed_sources
CSP_SCRIPT_SRC = ["'self'", "'unsafe-inline'"] + csp_allowed_sources
CSP_FONT_SRC = ["'self'"] + csp_allowed_sources
CSP_IMG_SRC = ["'self'"] + csp_allowed_sources
CSP_FRAME_SRC = ["'self'"] + csp_allowed_sources
CSP_FRAME_ANCESTORS = ["'self'"] + csp_allowed_iframe_sites

try:
    from .local import *
except ImportError:
    pass
