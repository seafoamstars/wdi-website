function toggleLanguageMessage() {
  let x = document.getElementById('no-languages-notification')
  console.log('Before Click:' + x.style.display)
  if (x.style.display === 'none') {
    x.style.display = 'block'
  } else {
    x.style.display = 'none'
  }
  console.log('After Click:' + x.style.display)
}
