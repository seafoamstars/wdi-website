import captcha
from django.test import TransactionTestCase, TestCase
import sys
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile

from whrc.models.models import Signature, Organization
from whrc.forms import SignatureWithOrganizationForm

class TestSignatureForm(TransactionTestCase):

    def setUp(self):
        captcha.conf.settings.CAPTCHA_TEST_MODE = True  # Make sure captcha does not get in the way
        self.valid_data_examples = [
            {
                "name": "A" * 255,
                "email": "jdoe@example.com",
                "subscribe_to_emails": True,
                "organization": "BigOrg inc.",
                "sign_for_organization": False,
                "country": "GB",
                "province": "",
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Klara Bella",
                "email": "klara@bella.nl",
                "subscribe_to_emails": False,
                "organization": "",
                "sign_for_organization": False,
                "country": "NL",
                "province": "",
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Маша Иванова",
                "email": "mar4ik_ivvvanoffffa@mail.ru",
                "subscribe_to_emails": False,
                "organization": "<3",
                "sign_for_organization": False,
                "country": "RU",
                "province": "",
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Mary Smith",
                "email": "mary.smith@something.com",
                "subscribe_to_emails": True,
                "organization": "B" * 255,
                "sign_for_organization": True,
                "country": "US",
                "province": "",
                "international": False,
                "org_homepage": "http://www.feminisminc.com",
                "org_type": "OTHER",
                "permission_support": False,
                "permission_logo": False,
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
        ]

    def test_validate_valid_input(self):
        for valid_data in self.valid_data_examples:
            with self.subTest(name=valid_data["name"]):
                form = SignatureWithOrganizationForm(data=valid_data,
                        files=get_temporary_image(valid_data["sign_for_organization"]))
                self.assertTrue(form.is_valid())

    def test_validate_empty_email_fail(self):
        # Remove email from data examples
        empty_email_data_examples = self.valid_data_examples
        for data in empty_email_data_examples:
            data["email"] = ""

        for invalid_data in empty_email_data_examples:
            with self.subTest(name=invalid_data["name"]):
                form = SignatureWithOrganizationForm(data=invalid_data)
                self.assertFalse(form.is_valid())

    def test_validate_duplicate_email_fail(self):
        first_signature_data = {
            "name": "A" * 255,
            "email": "email@example.com",
            "subscribe_to_emails": True,
            "organization": "BigOrg inc.",
            "sign_for_organization": False,
            "country": "GB",
            "province": "",
            "captcha_0": "dummy",
            "captcha_1": "PASSED"
        }

        second_signature_data = {
            "name": "B" * 100,
            "email": "email@example.com",
            "subscribe_to_emails": False,
            "organization": "",
            "sign_for_organization": False,
            "country": "US",
            "province": "",
            "captcha_0": "dummy",
            "captcha_1": "PASSED"
        }

        form1 = SignatureWithOrganizationForm(data=first_signature_data,
                        files=get_temporary_image(first_signature_data["sign_for_organization"]))
        self.assertTrue(form1.is_valid())
        form1.save()

        # Trying to validate a form with different values but same email
        form2 = SignatureWithOrganizationForm(data=second_signature_data,
                        files=get_temporary_image(second_signature_data["sign_for_organization"]))
        self.assertFalse(form2.is_valid())

    def test_validate_incorrect_email_fail(self):
        invalid_data_examples = [
            {
                "name": "A" * 255,
                "email": "email_with_no_at",
                "subscribe_to_emails": True,
                "organization": "BigOrg inc.",
                "sign_for_organization": False,
                "country": "GB",
                "province": "",
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Klara Bella",
                "email": "@@@@@@@",
                "subscribe_to_emails": False,
                "organization": "",
                "sign_for_organization": False,
                "country": "NL",
                "province": "",
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Маша Иванова",
                "email": "<script>alert('Hi!');</script>",
                "subscribe_to_emails": False,
                "organization": "<3",
                "sign_for_organization": False,
                "country": "RU",
                "province": "",
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
        ]

        for invalid_data in invalid_data_examples:
            with self.subTest(name=invalid_data["name"]):
                form = SignatureWithOrganizationForm(data=invalid_data)
                self.assertFalse(form.is_valid())

    def test_validate_empty_org_fields_fail(self):
        invalid_data_examples = [
            {
                "name": "A" * 255,
                "email": "jdoe@example.com",
                "subscribe_to_emails": True,
                "organization": "",
                "sign_for_organization": True,
                "country": "GB",
                "province": "",
                "international": False,
                "org_homepage": "http://www.no-org-name.com",
                "org_type": "OTHER",
                "permission_support": False,
                "permission_logo": True,
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Klara Bella",
                "email": "klara@bella.nl",
                "subscribe_to_emails": False,
                "organization": "Feminism Inc.",
                "sign_for_organization": True,
                "country": "NL",
                "province": "",
                "international": False,
                "org_homepage": "",
                "org_type": "OTHER",
                "permission_support": False,
                "permission_logo": True,
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Маша Иванова",
                "email": "mar4ik_ivvvanoffffa@mail.ru",
                "subscribe_to_emails": False,
                "organization": "No Logo Organization",
                "sign_for_organization": True,
                "country": "RU",
                "province": "",
                "international": False,
                "org_homepage": "http://www.no-org-logo.com",
                "org_type": "OTHER",
                "permission_support": False,
                "permission_logo": False,
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
        ]

        for invalid_data in invalid_data_examples:
            with self.subTest(name=invalid_data["name"]):
                form = SignatureWithOrganizationForm(data=invalid_data,
                        files=get_temporary_image(invalid_data["permission_logo"]))
                self.assertFalse(form.is_valid())

    def test_validate_empty_name_fail(self):
        # Remove name from data examples
        invalid_data_examples = self.valid_data_examples
        for data in invalid_data_examples:
            data["name"] = ""

        for invalid_data in invalid_data_examples:
            with self.subTest(name=invalid_data["email"]):
                form = SignatureWithOrganizationForm(data=invalid_data)
                self.assertFalse(form.is_valid())

    def test_validate_empty_country_fail(self):
        # Remove country from data examples
        invalid_data_examples = self.valid_data_examples
        for data in invalid_data_examples:
            data["country"] = ""

        for invalid_data in invalid_data_examples:
            with self.subTest(name=invalid_data["name"]):
                form = SignatureWithOrganizationForm(data=invalid_data)
                self.assertFalse(form.is_valid())

    def test_validate_empty_org_fields_fail(self):
        invalid_data_examples = [
            {
                "name": "A" * 255,
                "email": "jdoe@example.com",
                "subscribe_to_emails": True,
                "organization": "",
                "sign_for_organization": True,
                "country": "GB",
                "province": "",
                "international": False,
                "org_homepage": "http://www.no-org-name.com",
                "org_type": "OTHER",
                "permission_support": False,
                "permission_logo": True,
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Klara Bella",
                "email": "klara@bella.nl",
                "subscribe_to_emails": False,
                "organization": "Feminism Inc.",
                "sign_for_organization": True,
                "country": "NL",
                "province": "",
                "international": False,
                "org_homepage": "",
                "org_type": "OTHER",
                "permission_support": False,
                "permission_logo": True,
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Маша Иванова",
                "email": "mar4ik_ivvvanoffffa@mail.ru",
                "subscribe_to_emails": False,
                "organization": "No Logo Organization",
                "sign_for_organization": True,
                "country": "RU",
                "province": "",
                "international": False,
                "org_homepage": "http://www.no-org-logo.com",
                "org_type": "OTHER",
                "permission_support": False,
                "permission_logo": False,
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
        ]

        for invalid_data in invalid_data_examples:
            with self.subTest(name=invalid_data["name"]):
                form = SignatureWithOrganizationForm(data=invalid_data,
                        files=get_temporary_image(invalid_data["permission_logo"]))
                self.assertFalse(form.is_valid())

    def test_save_valid_input(self):
        for valid_data in self.valid_data_examples:
            with self.subTest(name=valid_data["name"]):
                form = SignatureWithOrganizationForm(data=valid_data,
                        files=get_temporary_image(valid_data["sign_for_organization"]))
                form.save_with_language("EN")

                saved_signature = Signature.objects.filter(email=valid_data["email"])[0]
                self.assertEquals(saved_signature.name, valid_data["name"])
                self.assertEquals(saved_signature.country, valid_data["country"])
                self.assertEquals(str(saved_signature.province_id or ''), valid_data["province"])
                self.assertEquals(saved_signature.subscribe_to_emails, valid_data["subscribe_to_emails"])
                self.assertEquals(saved_signature.organization, valid_data["organization"])
                self.assertEquals(saved_signature.sign_for_organization, valid_data["sign_for_organization"])

                if valid_data["sign_for_organization"]:
                    saved_organization = Organization.objects.filter(name=valid_data["organization"])[0]
                    self.assertEquals(saved_organization.type, valid_data["org_type"])
                    self.assertEquals(saved_organization.country, valid_data["country"])
                    self.assertEquals(saved_organization.international, valid_data["international"])
                    self.assertEquals(saved_organization.homepage, valid_data["org_homepage"])
                    self.assertEquals(saved_organization.contact_email, valid_data["email"])
                    self.assertEquals(saved_organization.permission_support, valid_data["permission_support"])
                    self.assertEquals(saved_organization.permission_logo, valid_data["permission_logo"])
                    self.assertEquals(saved_organization.confirmed, False)

    def test_save_with_language_valid_input(self):
        languages = ["EN", "RU", "ES"]
        lang_index = 0
        for valid_data in self.valid_data_examples:
            language = languages[lang_index]
            lang_index = (lang_index + 1) % len(languages)

            with self.subTest(name=valid_data["name"], language=language):
                form = SignatureWithOrganizationForm(data=valid_data,
                        files=get_temporary_image(valid_data["sign_for_organization"]))
                form.save_with_language(language_code=language)

                saved_signature = Signature.objects.filter(email=valid_data["email"])[0]
                self.assertEquals(saved_signature.language, language)

class TestProvinceField(TestCase):
    def setUp(self):
        captcha.conf.settings.CAPTCHA_TEST_MODE = True  # Make sure captcha does not get in the way
        self.valid_data_examples = [
            {
                "name": "A" * 255,
                "email": "jdoe@example.com",
                "subscribe_to_emails": True,
                "organization": "BigOrg inc.",
                "sign_for_organization": False,
                "country": "GB",
                "province": "4565",
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Klara Bella",
                "email": "klara@bella.nl",
                "subscribe_to_emails": False,
                "organization": "",
                "sign_for_organization": False,
                "country": "NL",
                "province": "",
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Маша Иванова",
                "email": "mar4ik_ivvvanoffffa@mail.ru",
                "subscribe_to_emails": False,
                "organization": "<3",
                "sign_for_organization": False,
                "country": "RU",
                "province": "2765",
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
            {
                "name": "Mary Smith",
                "email": "mary.smith@something.com",
                "subscribe_to_emails": True,
                "organization": "B" * 255,
                "sign_for_organization": True,
                "country": "US",
                "province": "",
                "international": False,
                "org_homepage": "http://www.feminisminc.com",
                "org_type": "OTHER",
                "permission_support": False,
                "permission_logo": False,
                "captcha_0": "dummy",
                "captcha_1": "PASSED"
            },
        ]

    def test_validate_valid_input(self):
        for valid_data in self.valid_data_examples:
            with self.subTest(name=valid_data["name"]):
                form = SignatureWithOrganizationForm(data=valid_data,
                        files=get_temporary_image(valid_data["sign_for_organization"]))
                self.assertTrue(form.is_valid())

    def test_save_valid_input(self):
        for valid_data in self.valid_data_examples:
            with self.subTest(name=valid_data["name"]):
                form = SignatureWithOrganizationForm(data=valid_data,
                        files=get_temporary_image(valid_data["sign_for_organization"]))
                form.save_with_language("EN")

                saved_signature = Signature.objects.filter(email=valid_data["email"])[0]
                self.assertEquals(saved_signature.name, valid_data["name"])
                self.assertEquals(saved_signature.country, valid_data["country"])
                self.assertEquals(str(saved_signature.province_id or ''), valid_data["province"])
                self.assertEquals(saved_signature.subscribe_to_emails, valid_data["subscribe_to_emails"])
                self.assertEquals(saved_signature.organization, valid_data["organization"])
                self.assertEquals(saved_signature.sign_for_organization, valid_data["sign_for_organization"])

                if valid_data["sign_for_organization"]:
                    saved_organization = Organization.objects.filter(name=valid_data["organization"])[0]
                    self.assertEquals(saved_organization.type, valid_data["org_type"])
                    self.assertEquals(saved_organization.country, valid_data["country"])
                    self.assertEquals(saved_organization.international, valid_data["international"])
                    self.assertEquals(saved_organization.homepage, valid_data["org_homepage"])
                    self.assertEquals(saved_organization.contact_email, valid_data["email"])
                    self.assertEquals(saved_organization.permission_support, valid_data["permission_support"])
                    self.assertEquals(saved_organization.permission_logo, valid_data["permission_logo"])
                    self.assertEquals(saved_organization.confirmed, False)

    def test_save_with_language_valid_input(self):
        languages = ["EN", "RU", "ES"]
        lang_index = 0
        for valid_data in self.valid_data_examples:
            language = languages[lang_index]
            lang_index = (lang_index + 1) % len(languages)

            with self.subTest(name=valid_data["name"], language=language):
                form = SignatureWithOrganizationForm(data=valid_data,
                        files=get_temporary_image(valid_data["sign_for_organization"]))
                form.save_with_language(language_code=language)

                saved_signature = Signature.objects.filter(email=valid_data["email"])[0]
                self.assertEquals(saved_signature.language, language)

def get_temporary_image(sign_for_organization):
    if not(sign_for_organization):
        return None
    io = BytesIO()
    size = (200, 200)
    color = (255, 0, 0, 0)
    image = Image.new("RGBA", size, color)
    image.save(io, format="PNG")
    image_file = InMemoryUploadedFile(io, "ImageField", "logo-%d.png" % id(image), "image/png", sys.getsizeof(io), None)
    image_file.seek(0)
    return { "org_logo": image_file }
