from django.test import TestCase
from wagtail.core.models import Page, Site

from whrc.models.models import Signature, SignatureConfirmationToken
from whrc.models.pages import StandardPage


def make_test_signature_with_token():
    signature = Signature(
        name="Test",
        country="US",
        email="test123@example.com",
        subscribe_to_emails=False,
        sign_for_organization=False,
        language="en"
    )
    signature.save()

    token = SignatureConfirmationToken(signature=signature, token="1" * 32)
    token.save()

    return signature, token


def make_one_standard_page(title="Test", slug="test", hostname="womensdeclaration.com"):
    root_page = Page(title="Root", depth=1, path="/")
    root_page.save()
    page = StandardPage(title=title, slug=slug)
    root_page.add_child(instance=page)
    page.set_url_path(parent=root_page)
    page.save()

    test_site = Site(hostname=hostname, root_page=root_page)
    test_site.save()


class TestErrors(TestCase):
    def test_not_found_says_sorry(self):
        resp = self.client.get("/something/", follow=True)

        self.assertContains(resp, "Sorry", status_code=404)


class TestVerifySignature(TestCase):

    def test_verify_signature_invalid_token_404(self):
        resp = self.client.get("/verify/WRONG", follow=True)

        self.assertRedirects(resp, '/en/verify/WRONG/', target_status_code=404)

    def test_verify_signature_nonexistent_token_redirect_home_with_message(self):
        resp = self.client.get("/verify/00000000000000000000000000000000", follow=True)

        self.assertRedirects(resp, '/en/')

        cookie = self.client.cookies["messages"].value
        self.assertTrue("The confirmation code you've tried to use is invalid." in cookie)

    def test_verify_signature_already_verified(self):
        test_signature, test_token = make_test_signature_with_token()
        resp = self.client.get("/verify/" + test_token.token) # First verification should be successful
        resp = self.client.get("/verify/" + test_token.token, follow=True)

        self.assertRedirects(resp, '/en/')

        cookie = self.client.cookies["messages"].value
        self.assertTrue("You have already confirmed your signature before." in cookie)

    def test_verify_signature_extra_char(self):
        test_signature, test_token = make_test_signature_with_token()
        resp = self.client.get("/verify/" + test_token.token + "1" , follow=True)

        self.assertRedirects(resp, '/en/verify/' + test_token.token + '1/', target_status_code=404)

    def test_verify_signature_thankyou_message(self):
        test_signature, test_token = make_test_signature_with_token()
        resp = self.client.get("/verify/" + test_token.token, follow=True)

        self.assertRedirects(resp, '/en/')

        cookie = self.client.cookies["messages"].value
        self.assertTrue(
            "Thank you! You have confirmed your signature. It will now be visible on the signatories page." in cookie)

    def test_verify_signature_thankyou_page(self):
        test_signature, test_token = make_test_signature_with_token()
        hostname = "example.com"  # Wagtail expects a hostname to help ut route requests to appropriate Pages.
        make_one_standard_page(slug="thank-you", hostname=hostname)

        resp = self.client.get("/verify/" + test_token.token)

        self.assertRedirects(resp, str.format('http://{0}/en/thank-you/', hostname), status_code=302,
                             fetch_redirect_response=False)
