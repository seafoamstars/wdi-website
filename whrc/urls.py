from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls.i18n import i18n_patterns

from wagtail.admin import urls as wagtailadmin_urls
from wagtail.core import urls as wagtail_urls
from wagtail.documents import urls as wagtaildocs_urls

from search import views as search_views
from whrc.views import submit_signature as whrc_signature

urlpatterns = [

    url(r'^' + settings.ADMIN_NAME + r'/', include(wagtailadmin_urls)),
    url(r'^documents/', include(wagtaildocs_urls)),

    url(r'^search/$', search_views.search, name='search'),

    path('submit/signature/', whrc_signature.submit_signature),
    re_path('verify/(?P<input_token>[0-9a-fA-F]{32})$', whrc_signature.verify_signature),
    url(r'^captcha/', include('captcha.urls')),
    url(r'^chaining/', include('smart_selects.urls')),
    path('i18n/', include('django.conf.urls.i18n')),
]

urlpatterns += i18n_patterns(
    # For anything not caught by a more specific rule above, hand over to
    # Wagtail's page serving mechanism. This should be the last pattern in
    # the list:
    url(r'', include(wagtail_urls)),
)

handler404 = 'whrc.views.errors.view_404'
handler500 = 'whrc.views.errors.view_500'

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
