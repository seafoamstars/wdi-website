from urllib.parse import parse_qs, urlencode, urlparse, urlunparse

from bs4 import BeautifulSoup
from django.core.exceptions import ImproperlyConfigured
from wagtail.embeds.finders.oembed import OEmbedFinder

youtube_https = {
    "endpoint": "https://www.youtube.com/oembed",
    "urls": [
        r'^http(?:s)?://(?:[-\w]+\.)?youtube\.com/watch.+$',
        r'^http(?:s)?://(?:[-\w]+\.)?youtube\.com/v/.+$',
        r'^http(?:s)?://youtu\.be/.+$',
        r'^http(?:s)?://(?:[-\w]+\.)?youtube\.com/user/.+$',
        r'^http(?:s)?://(?:[-\w]+\.)?youtube\.com/[^#?/]+#[^#?/]+/.+$',
        r'^http(?:s)?://m\.youtube\.com/index.+$',
        r'^http(?:s)?://(?:[-\w]+\.)?youtube\.com/profile.+$',
        r'^http(?:s)?://(?:[-\w]+\.)?youtube\.com/view_play_list.+$',
        r'^http(?:s)?://(?:[-\w]+\.)?youtube\.com/playlist.+$',
    ],
}


class YouTubePreserveRelFinder(OEmbedFinder):
    """ OEmbed finder which converts to no-cookie form for YouTube URLs

    This finder operates on the youtube provider only, and
    
    (1) Changes the url to the no-cookie form
    (2) reproduces the source URL's rel=0 parameter if present
    (because YouTube's OEmbed API
    endpoint strips it from the formatted HTML it returns).
    """

    def __init__(self, providers=None, options=None):
        if providers is None:
            providers = [youtube_https]

        if providers != [youtube_https]:
            raise ImproperlyConfigured(
                'The YouTubePreserveRelFinder only operates on the youtube provider'
            )

        super().__init__(providers=providers, options=options)

    def find_embed(self, url, max_width=None):
        embed = super().find_embed(url, max_width)

        embed['html'] = embed['html'].replace(
            'youtube.com',
            'youtube-nocookie.com')

        embed['html'] = embed['html'].replace(
            'youtu.be',
            'youtube-nocookie.com')

        rel = parse_qs(urlparse(url).query).get('rel')
        if rel is not None:

            soup = BeautifulSoup(embed['html'], 'html.parser')
            iframe_url = soup.find('iframe').attrs['src']
            scheme, netloc, path, params, query, fragment = urlparse(iframe_url)
            querydict = parse_qs(query)
            if querydict.get('rel') != rel:
                querydict['rel'] = rel
                query = urlencode(querydict, doseq=1)

                iframe_url = urlunparse((scheme, netloc, path, params, query, fragment))
                soup.find('iframe').attrs['src'] = iframe_url
                embed['html'] = str(soup)

        return embed
