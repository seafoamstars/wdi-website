import csv

from django.core.exceptions import PermissionDenied
from django.http.response import HttpResponse

from whrc.models.models import Signature, Organization


def signatures_export_csv(request, confirmed=None, country=None, subscribed=None):
    """
    Export signatures in CSV format.
    """
    # Only give the signatures to those with View Signature AND Download Signature permissions
    if not (
        request.user.has_perm("whrc.view_signature")
        and request.user.has_perm("whrc.can_download_signatures")
    ):
        raise PermissionDenied

    # Make the response a downloadable CSV
    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="signatures.csv"'

    # Write the header row
    writer = csv.writer(response)
    writer.writerow(
        [
            "\ufeffName",
            "Email",
            "Email confirmed",
            "Subscribe to emails",
            "Organization",
            "Sign for organization",
            "Country",
            "State/Province",
            "Timestamp",
        ]
    )

    if confirmed == "1" or confirmed == "0":
        signatures_request = Signature.objects.filter(
            email_confirmed=bool(int(confirmed))
        )
    else:  # By default we only want confirmed signatures.
        signatures_request = Signature.objects.filter(email_confirmed=True)

    if subscribed == "1" or subscribed == "0":
        signatures_request = signatures_request.filter(
            subscribe_to_emails=bool(int(subscribed))
        )

    if country:
        signatures_request = signatures_request.filter(country=country)

    # Create a row for each signature
    for signature in signatures_request.all():
        writer.writerow(
            [
                signature.name,
                signature.email,
                signature.email_confirmed,
                signature.subscribe_to_emails,
                signature.organization,
                signature.sign_for_organization,
                signature.country,
                signature.province,
                signature.timestamp,
            ]
        )

    return response


def organizations_export_csv(
    request,
    confirmed=None,
    country=None,
    international=None,
    permission_support=None,
    permission_logo=None,
    type=None,
):

    print(type)
    print(country)
    """
    Export organizations in CSV format.
    """
    # Only give the organizations to those with View Organization permissions
    if not request.user.has_perm("whrc.view_organization"):
        raise PermissionDenied

    # Make the response a downloadable CSV
    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="organizations.csv"'

    # Write the header row
    writer = csv.writer(response)
    writer.writerow(
        [
            "\ufeffName",
            "Type",
            "Country",
            "International",
            "Homepage",
            "Contact Email",
            "Permission to mention Support",
            "Permission to use Logo",
            "Confirmed",
        ]
    )

    if confirmed == "1" or confirmed == "0":
        orgs_request = Organization.objects.filter(confirmed=bool(int(confirmed)))
    else:  # By default we only want confirmed organizations.
        orgs_request = Organization.objects.filter(confirmed=True)

    if international == "1" or international == "0":
        orgs_request = orgs_request.filter(international=bool(int(international)))

    if permission_support == "1" or permission_support == "0":
        orgs_request = orgs_request.filter(
            permission_support=bool(int(permission_support))
        )

    if permission_logo == "1" or permission_logo == "0":
        orgs_request = orgs_request.filter(permission_logo=bool(int(permission_logo)))

    if country:
        orgs_request = orgs_request.filter(country=country)

    if type:
        orgs_request = orgs_request.filter(type=type)

    # Create a row for each organization
    for organization in orgs_request.all():
        writer.writerow(
            [
                organization.name,
                organization.type,
                organization.country,
                organization.international,
                organization.homepage,
                organization.contact_email,
                organization.permission_support,
                organization.permission_logo,
                organization.confirmed,
            ]
        )

    return response
