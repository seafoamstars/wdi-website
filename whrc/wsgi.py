"""
WSGI config for Women's Declaration project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/
"""

import os
from threading import Thread

from django.conf import settings
from django.core.wsgi import get_wsgi_application


def update_all_mailing_lists():
    if not hasattr(settings, 'MAILER_API_KEY'):
        print("Automatic signatures sync disabled. If you want to automatically syncronize signatures with a mailing list, set a variable MAILER_API_KEY in project settings.")
        return

    from time import sleep
    sleep(15)

    from whrc.management.commands.update_mailing_lists import Command
    command = Command()
    command.handle(country_ids=['all'])

    sleep(60*60*3)

    country_codes_1 = ["GB", "AU", "DE", "NL", "AR", "IN", "FR", "SK", "SG", "GR"]
    country_codes_2 = ["US", "RU", "MX", "ES", "BR", "IT", "CA", "KR", "DK", "SE"]
    country_codes_3 = ["BO", "HR", "IE", "NZ", "IR", "IS", "PE", "PT", "RS", "UA"]
    country_codes_4 = ["ALL"]

    while True:
        command.handle(country_ids=country_codes_1)
        sleep(60*60*2)
        command.handle(country_ids=country_codes_2)
        sleep(60*60*2)
        command.handle(country_ids=country_codes_3)
        sleep(60*60*2)
        command.handle(country_ids=country_codes_4)
        sleep(60*60*2)


# === wsgi logic starts here ===

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "whrc.settings.dev")

runner = Thread(target=update_all_mailing_lists)
runner.start()

application = get_wsgi_application()
